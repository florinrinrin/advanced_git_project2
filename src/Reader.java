import java.io.*;

public class Reader {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("file.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("fileLibrary.txt"));


        Publication[] publication = new Publication[100];
        String line;
        int count = 0;
        while ((line = reader.readLine()) != null) {
            String[] infos = line.split("~");
            if (infos[0].equals("B")) {
                String name = infos[1];
                String author = infos[2];
                String genre = infos[3];
                String ISBN = infos[4];
                Books b = new Books(name,author,genre,ISBN);
                publication[count] = b;
                count++;
            } else if (infos[0].equals("M")){
                String title = infos[1];
                String genre = infos[2];
                String participatingActors = infos[3];
                String idIMDB = infos[4];
                Movies m = new Movies(title,genre,participatingActors,idIMDB);
                publication[count] = m;
                count++;
            } else if (infos[0].equals("S")){
                String title = infos[1];
                String author = infos[2];
                String theme = infos[3];
                String DOI = infos[4];
                ScientificArticles m = new ScientificArticles(title,author,theme,DOI);
                publication[count] = m;
                count++;)
        }

        for (int i = 0; i < products.length; i++) {
            if (products[i] != null){
                System.out.println(products[i].getName());
                writer.write(products[i].getName());
                writer.write('\n');}
        }


        reader.close();
        writer.close();
    }

}

public class ScientificArticles extends Publication {
    private String theme;
    private String DOI;
    private String author;

    public ScientificArticles(String title, String author, String theme, String DOI) {
        super(title);
        this.author=author;
        this.theme=theme;
        this.DOI=DOI;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}

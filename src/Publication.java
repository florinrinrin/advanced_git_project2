public class Publication {
    private String title;


    public Publication(String title){
        this.title=title;
    }


    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

public class Books extends Publication {
    public String genre;
    public String ISBN;
    private String author;

    public Books(String title, String author,String genre,String ISBN) {
        super(title);
        this.author=author;
        this.genre=genre;
        this.ISBN =ISBN;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}

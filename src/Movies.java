public class Movies extends Publication {

    private String genre;
    private String participatingActors;
    private String idIMDB;

    public Movies(String title, String genre,String participatingActors, String idIMDB) {
        super(title);
        this.genre=genre;
        this.participatingActors=participatingActors;
        this.idIMDB=idIMDB;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getParticipatingActors() {
        return participatingActors;
    }

    public void setParticipatingActors(String participatingActors) {
        this.participatingActors = participatingActors;
    }

    public String getIdIMDB() {
        return idIMDB;
    }

    public void setIdIMDB(String idIMDB) {
        this.idIMDB = idIMDB;
    }
}
